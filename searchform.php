<?php ?>

<form action="/" method="get" class="search-form">
	<input type="search" class="search-field" placeholder="Search" name="search" title="Search" value="<?php the_search_query(); ?>" />
	<input type="submit" class="search-submit" value="Search" />
</form>