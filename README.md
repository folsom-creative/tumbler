Tumbler
===

Tumbler is a starter theme that includes uses Sass and a few Gulp tasks to create most of the theme structure. Add any node_modules or bower_compnents and then copy them into a lib folder for easy usage. 

The framework is setup to use Bourbon and Neat.

Todos:
- General theme cleanup.`
- Ensure _s code (or the word cti) is removed and updated to tumbler.
- Fix variables and default margins and paddings.
- Cleanup customizer features (add them).